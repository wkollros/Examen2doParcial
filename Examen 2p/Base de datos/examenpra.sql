-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-05-2018 a las 10:49:09
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examenpra`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `adm_dni` int(11) NOT NULL,
  `adm_email` varchar(256) NOT NULL,
  `adm_username` varchar(256) NOT NULL,
  `adm_password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id_admin`, `adm_dni`, `adm_email`, `adm_username`, `adm_password`) VALUES
(1, 8847306, 'admin@gmail.com', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comp_question`
--

CREATE TABLE `comp_question` (
  `id_question_comp` int(11) NOT NULL,
  `comp_question` text NOT NULL,
  `comp_answer` text NOT NULL,
  `id_quiz` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comp_question`
--

INSERT INTO `comp_question` (`id_question_comp`, `comp_question`, `comp_answer`, `id_quiz`) VALUES
(1, 'Programamos usando el motor _____', ' visual code', 1),
(2, 'HTML es parte del ____ de una pagina.', ' frontend', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mo_question`
--

CREATE TABLE `mo_question` (
  `id_question_mo` int(11) NOT NULL,
  `mo_quest` text NOT NULL,
  `mo_o1` text NOT NULL,
  `mo_o2` text NOT NULL,
  `mo_o3` text NOT NULL,
  `mo_o4` text NOT NULL,
  `mo_answer` text NOT NULL,
  `id_quiz` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mo_question`
--

INSERT INTO `mo_question` (`id_question_mo`, `mo_quest`, `mo_o1`, `mo_o2`, `mo_o3`, `mo_o4`, `mo_answer`, `id_quiz`) VALUES
(1, 'En que hacemos el backend?', ' HTML', 'PHP', 'JAVASCRIPT', 'CSS', 'PHP', 1),
(2, 'En que hacemos el frontend?', ' HTML', 'CSS', 'PHP', 'C#', 'HTML', 1),
(3, 'De que es este curso?', ' C#', 'HTML', 'JAVASCRIPT', 'VISUAL BASIC', 'HTML', 2),
(4, 'De que es este curso?', ' HTML', 'JAVASCRIPT', 'CSS', 'C#', 'JAVASCRIPT', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quiz`
--

CREATE TABLE `quiz` (
  `id_quiz` int(11) NOT NULL,
  `quiz_time` double NOT NULL,
  `quiz_description` text NOT NULL,
  `quiz_link` text NOT NULL,
  `id_signature` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `quiz`
--

INSERT INTO `quiz` (`id_quiz`, `quiz_time`, `quiz_description`, `quiz_link`, `id_signature`) VALUES
(1, 10, ' Este es el cuestionario de php.', 'http://localhost/examen2doparcial/cuestionariophp.php', 1),
(2, 5, ' Este es el cuestionario de HTML', 'http://localhost/examen2doparcial/cuestionariohtml.php', 2),
(4, 5, ' Este es el cuestionario de JavaScript.', 'http://localhost/examen2doparcial/cuestionariojavascript.php', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `signature`
--

CREATE TABLE `signature` (
  `id_signature` int(11) NOT NULL,
  `sig_name` varchar(256) NOT NULL,
  `sig_description` text NOT NULL,
  `sig_imgurl` varchar(256) NOT NULL,
  `sig_pageurl` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `signature`
--

INSERT INTO `signature` (`id_signature`, `sig_name`, `sig_description`, `sig_imgurl`, `sig_pageurl`) VALUES
(1, 'PHP', ' Este es un curso avanzado de php.', 'http://localhost/examen2doparcial/clases/php.jpg', 'http://localhost/examen2doparcial/cursophp.php'),
(2, 'HTML', ' Este es un curso basico de html.', 'http://localhost/examen2doparcial/clases/html.jpg', 'http://localhost/examen2doparcial/cursohtml.php'),
(3, 'JavaScript', ' Este es un curso basico de javascript.', 'http://localhost/examen2doparcial/clases/javascipt.jpg', 'http://localhost/examen2doparcial/cursojavascript.php'),
(4, 'Gastronomia', ' Curso optativo de gastronomia. ', 'http://localhost/examen2doparcial/clases/gastronomia.jpg', 'http://localhost/examen2doparcial/cursogastronomia.php');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `student`
--

CREATE TABLE `student` (
  `id_student` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `student`
--

INSERT INTO `student` (`id_student`, `dni`, `email`, `username`, `password`) VALUES
(5, 8847309, ' berno@gmail.com', 'berno', '$2y$10$.QstulcOIYvi336mQ7QN5.dWLqHx9u.RWgG0ry/iJKfL7w1swabUq'),
(6, 8847305, ' bernilla@gmail.com', 'berna', '$2y$10$tCBGGDrjkbraFzSqE5GROeaLvpEilv7QEMW9Jr771QbAaVtd7KsKu');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `theo_question`
--

CREATE TABLE `theo_question` (
  `id_question_theo` int(11) NOT NULL,
  `theo_ques` text NOT NULL,
  `theo_answer` text NOT NULL,
  `id_quiz` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `theo_question`
--

INSERT INTO `theo_question` (`id_question_theo`, `theo_ques`, `theo_answer`, `id_quiz`) VALUES
(1, 'Cuantos dedos tienes?', ' cinco', 2),
(2, 'Cuantas manos tienes?', ' dos', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `comp_question`
--
ALTER TABLE `comp_question`
  ADD PRIMARY KEY (`id_question_comp`);

--
-- Indices de la tabla `mo_question`
--
ALTER TABLE `mo_question`
  ADD PRIMARY KEY (`id_question_mo`);

--
-- Indices de la tabla `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id_quiz`);

--
-- Indices de la tabla `signature`
--
ALTER TABLE `signature`
  ADD PRIMARY KEY (`id_signature`);

--
-- Indices de la tabla `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id_student`);

--
-- Indices de la tabla `theo_question`
--
ALTER TABLE `theo_question`
  ADD PRIMARY KEY (`id_question_theo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `comp_question`
--
ALTER TABLE `comp_question`
  MODIFY `id_question_comp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `mo_question`
--
ALTER TABLE `mo_question`
  MODIFY `id_question_mo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id_quiz` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `signature`
--
ALTER TABLE `signature`
  MODIFY `id_signature` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `student`
--
ALTER TABLE `student`
  MODIFY `id_student` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `theo_question`
--
ALTER TABLE `theo_question`
  MODIFY `id_question_theo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
