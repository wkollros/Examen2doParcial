<?php
session_start();
    if(isset($_POST['submit'])){

        include 'dbh-inc.php';

        $username=mysqli_real_escape_string($conn, $_POST['username']);
        $password=mysqli_real_escape_string($conn, $_POST['password']);  

        //Controlando errores
        //ver si dejaron espacios vacios
        if (empty($username) || empty($password)){
            header("Location: ../index.php?Login=Empty");
            exit();
            }else{
                    //primero veremos si es un admin
                
                $sql = "SELECT * FROM admin WHERE adm_username = '$username'";
                $result = mysqli_query($conn,$sql);
                $resultcheck= mysqli_num_rows($result);
                if ($resultcheck<1)
                {
                 //osea si no es admin q hacer? ver si es estudiante!
                 //codigo para ver si es un estudiante y demas
                    $sql = "SELECT * FROM student WHERE username = '$username'";
                    $result = mysqli_query($conn,$sql);
                    $resultcheck= mysqli_num_rows($result);
                    if ($resultcheck<1)
                    {
                        header("Location: ../index.php?Login=Noexisteusername");
                        exit();
                        //es mas seguro no decirle al usuario donde se equivoco para que no adivine valores tratando de entrar
                    }else{
                        if ($row = mysqli_fetch_assoc($result)){
                            //dehashing la contraseña
                            $hashedpasswordcheck=password_verify($password,$row['password']);
                            if($hashedpasswordcheck == false)
                            {
                                header("Location: ../index.php?Login=errorcontraseña");
                                exit();
                            }elseif ($hashedpasswordcheck== true) {      //necesitamos usar este elseif para asegurarnos q no pase nada raro
                                //aqui logeamos al usuario
                                //para que funcione el $_SESSION debemos tener una sesion iniciada corriendo en nuestra pagina
                                $_SESSION['u_username'] = $row['username'];
                                $_SESSION['u_email'] = $row['email'];
                                $_SESSION['u_dni'] = $row['dni'];
                                $_SESSION['u_password'] = $row['password'];
                                $_SESSION['u_id'] = $row['id_student'];
                                header("Location: ../index.php?Login=Success");
                                exit();
                            }
                        }
                    }
                 


                    
                    
                }else{
                    //entonces si SI es un admin q hacemos
                    if ($row = mysqli_fetch_assoc($result)){
                        //dehashing la contraseña                        
                        if($password!=$row['adm_password'])
                        {
                            header("Location: ../index.php?Login=errorcontraseña");
                            exit();
                        }else{      //necesitamos usar este elseif para asegurarnos q no pase nada raro
                            //aqui logeamos al usuario
                            //para que funcione el $_SESSION debemos tener una sesion iniciada corriendo en nuestra pagina
                            $_SESSION['a_username'] = $row['adm_username'];
                            $_SESSION['a_email'] = $row['adm_email'];
                            $_SESSION['a_dni'] = $row['adm_dni'];
                            $_SESSION['a_password'] = $row['adm_password'];
                            $_SESSION['a_id'] = $row['id_admin'];
                            header("Location: ../index.php?Login=Success");
                            exit();
                        }
                    }
                }









                /*
                //codigo para ver si es un estudiante y demas
                $sql = "SELECT * FROM student WHERE username = '$username'";
                $result = mysqli_query($conn,$sql);
                $resultcheck= mysqli_num_rows($result);
                if ($resultcheck<1)
                {
                    header("Location: ../index.php?Login=Noexisteusername");
                    exit();
                    //es mas seguro no decirle al usuario donde se equivoco para que no adivine valores tratando de entrar
                }else{
                    if ($row = mysqli_fetch_assoc($result)){
                        //dehashing la contraseña
                        $hashedpasswordcheck=password_verify($password,$row['password']);
                        if($hashedpasswordcheck == false)
                        {
                            header("Location: ../index.php?Login=errorcontraseña");
                            exit();
                        }elseif ($hashedpasswordcheck== true) {      //necesitamos usar este elseif para asegurarnos q no pase nada raro
                            //aqui logeamos al usuario
                            //para que funcione el $_SESSION debemos tener una sesion iniciada corriendo en nuestra pagina
                            $_SESSION['u_username'] = $row['username'];
                            $_SESSION['u_email'] = $row['email'];
                            $_SESSION['u_dni'] = $row['dni'];
                            $_SESSION['u_password'] = $row['password'];
                            $_SESSION['u_id'] = $row['id_student'];
                            header("Location: ../index.php?Login=Success");
                            exit();
                        }
                    }
                }
                */


            }
        }else
        {
            header("Location: ../index.php?Login=Error");
            exit();

        }
?>