<?php
    // vemos si realmente hicieron click al boton
    if(isset($_POST['submit'])){
        include_once 'dbh-inc.php';
        $qui_time = mysqli_real_escape_string($conn, $_POST['qui_time']);
        $qui_description = mysqli_real_escape_string($conn, $_POST['qui_description']);
        $qui_link = mysqli_real_escape_string($conn, $_POST['qui_link']);
        $id_signature = mysqli_real_escape_string($conn, $_POST['id_signature']);        
        //Control de errores
        //Evitar espacios en blanco
        if(empty($qui_time) || empty($qui_description) || empty($id_signature))
        {
            //header("Location: ../crearcuestionario.php?id_materia=$id_signature");            
            header("Location: ../index.php?create=empty");
            exit();
        } else {                    
                        $sql = "SELECT * FROM quiz WHERE id_signature=$id_signature";
                        $result = mysqli_query($conn,$sql);
                        $resultCheck= mysqli_num_rows($result);
                        if ($resultCheck > 0) {
                            header("Location: ../index.php?create=alredy_exists");
                            exit();
                        } else{
                            $sql="INSERT INTO quiz (quiz_time,quiz_description,quiz_link,id_signature) 
                            VALUES ('$qui_time',' $qui_description','$qui_link','$id_signature');";
                            mysqli_query($conn,$sql);
                            header("Location: ../index.php?create=success!");
                            exit();
                   }                    
               }           
    }    
    else{
        header("Location: ../crearcuestionario.php?=Error");
        exit(); 
    }
?>