<?php
// vemos si realmente hicieron click al boton
    if(isset($_POST['submit'])){
        include_once 'dbh-inc.php';

        $dni = mysqli_real_escape_string($conn, $_POST['dni']);
        $email = mysqli_real_escape_string($conn, $_POST['email']);
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $password1 = mysqli_real_escape_string($conn, $_POST['password1']);
        $password2 = mysqli_real_escape_string($conn, $_POST['password2']);
        
        //mysqli_real_scape_string hace que si o si lo que esta 
        //entrando sea cadena 
        //y no puedan meter codigo, es para seguridad de nuestro sistema

        //Control de errores
        //Evitar espacios en blanco
        if(empty($dni) || empty($email) || empty($password1) 
        || empty($password2))
        {
            header("Location: ../crearcuenta.php?signup=empty");
            exit();
        } else {
           //veremos si los caracteres que entran son validos
           if(!preg_match("/^[0-9]*$/",$dni)){
            header("Location: ../crearcuenta.php?signup=invalid");
            exit();
           }else {
               //veremos si ambas contraseñas son iguales
               if ($password1!=$password2)
               {
                header("Location: ../crearcuenta.php?signup=passwords_dont_match");
                exit();
               } else {
                    //Veremos si el email es valido
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                        header("Location: ../crearcuenta.php?signup=invalidemail");
                        exit();
                    } else {
                        //veremos si la persona ya esta registrada con el dni
                        $sql = "SELECT * FROM student WHERE dni='$dni'";
                        $result = mysqli_query($conn,$sql);
                        $resultCheck= mysqli_num_rows($result);
                        if ($resultCheck > 0) {
                            header("Location: ../crearcuenta.php?signup=dniexists");
                            exit();
                        } else{
                        //veremos si ya existe el username
                        $sql = "SELECT * FROM student WHERE username='$username'";
                        $result = mysqli_query($conn,$sql);
                        $resultCheck= mysqli_num_rows($result);
                        //estamos viendo si es que tenemos alguna linea de resultado, si tenemos algun resultado damos error
                        if ($resultCheck > 0) {
                            header("Location: ../crearcuenta.php?signup=usertaken");
                            exit();
                        } else{
                            //Hashing la contraseñaa
                            $hashedpasssword = password_hash($password1, PASSWORD_DEFAULT);
                            //insertando el usuario en la base de datos
                            $sql="INSERT INTO student (dni,email,username,password) VALUES ('$dni',' $email','$username','$hashedpasssword');";

                            mysqli_query($conn,$sql);
                            header("Location: ../index.php?signup=Success");
                            exit();

                   }
                }    
               }    
            }
           }
        }
    }
    //aqui indicamos que hacer si es que el usuario quiere entrar
    //directamente usando la url
    else{
        header("Location: ../crearcuenta.php");
        exit(); //cierra y detiene el script de correr
    }


    //Otro metodo
    /*
    //tratando de conectar con la base de datos
    $con=mysqli_connect('127.0.0.1','root','');
    //aqui verificamos que nos hayamos conectado
    if(!$con)
    {
        echo "No se conecto a la base de datos";
    }
    //aqui nos hemos conectado
    //ahora verificamos si esta seleccionando la base de datos
    if(!mysqli_select_db($con,'examenpra')){
        echo "Base de datos no seleccionada";
    }
    //aqui consideramos que ya selecciono la base de datos
    //ahora recibimos los datos del form
    $dni = $_POST['dni'];
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password1 = $_POST['password1'];
    $password2 = $_POST['password2'];
    //verificando que ammbas contraseñas sean iguales
    if ($dni=="" || $email=="" || $username=="" || $password1=="" || $password2==""){
        echo "Error, un campo quedo sin llenar.";
        header("refresh:2;url=http://localhost/examen2doparcial/crearcuenta.php");
    }
    else{

    
        if($password1!=$password2)
        {
            echo "Las contraseñas no coinciden";
            header("refresh:2;url=http://localhost/examen2doparcial/crearcuenta.php");
        }
        //SI AMBAS CONTRASENHAS SON IGUALES hacemos lo siguiente
        else{
            $sql="Insert into student (dni,email,username,password) values ('$dni','$email','$username','$password1')";
            //verificando si los valores no se insertaron
            if(!mysqli_query($con,$sql))
            {
                echo "No se insertaron los valores";
            }
            else
            {
                echo "Cuenta creada exitosamente";
            }
            //esto hace que nos redireccione automaticamente a otra pagina despues de los segundos que le digamos
            header("refresh:3;url=http://localhost/examen2doparcial/index.php");
            
        }
    }
    */


?>